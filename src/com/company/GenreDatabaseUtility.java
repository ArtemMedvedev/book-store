package com.company;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.sql.*;
import java.util.*;

public class GenreDatabaseUtility {
    private Scanner in = new Scanner(System.in);
    private ArrayList<Genre> listGenres;
    String jdbcUrl = "jdbc:mysql://localhost:3306/bookstore?" + "autoReconnect=true&useSSL=false";
    String dbusername = "root";
    String pass = "pass";
    Connection connection;

    public GenreDatabaseUtility() {
    }

    public ArrayList<Genre> addGenre(){
        boolean passed;
        String genrename = "";

        do {
            System.out.println("Input genre name");
            genrename = inputMore().toLowerCase();
            if (checkUniqueGenre(genrename)) {
                passed = true;
                System.out.println("Genre is already exist");
            } else {
                break;
            }
        } while (passed);

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            String sql = "INSERT INTO genre (name) VALUES ('" + genrename +  "'))";
            Statement statement = connection.createStatement();
            int rows = statement.executeUpdate(sql);
            if (rows > 0) {
                System.out.println("Genre added");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listGenres = getListOfGenres();
    }

    // check unique genre
    private boolean checkUniqueGenre(String genreName){
        String sql = "";
        boolean isExist = false;

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            sql = "SELECT * FROM genre WHERE name = '" + genreName + "'";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                isExist = true;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    public  List<Genre> getGenresFromDB(){
        List<Genre> genresFromDb = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            String sql = "SELECT * FROM genre";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            while (result.next()){
                String name = result.getString("name");;
                int id = result.getInt("id");
                Genre genre = new Genre(id, name);
                genresFromDb.add(genre);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genresFromDb;
    }

    public ArrayList<Genre> getListOfGenres(){
        listGenres = (ArrayList) getGenresFromDB();
        return listGenres;
    }

    public ArrayList<Genre> deleteGenre(int genreId){
       if (genreId !=0) {
           String s = listGenres.get(genreId).getName();
           try {
               connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
               String sql = "delete from genre where name='" + s + "'";
               Statement statement = connection.createStatement();
               int rows = statement.executeUpdate(sql);
               if (rows > 0) {
                   System.out.println("Genre deleted");
               }
               connection.close();
           } catch (SQLException e) {
               e.printStackTrace();
           }
       }
       return listGenres = getListOfGenres();
   }



    public void listGenres(){
        int i = 1;
        if (!listGenres.isEmpty()) {
            for (Genre g:listGenres) {
                System.out.println(i + ". " + g.getName());
                i++;
            }
        } else {
            System.out.println("No genres");
        }
    }

    public ArrayList<Genre> editGenre() {
        String userInput="";
        listGenres();

        System.out.println("**** Input genre id: ");
        int userChoose = validateInt()-1;
        String s = listGenres.get(userChoose).getName();

        System.out.println("New genre name " + s);
        if (in.hasNextLine()) {
            userInput = in.nextLine();
        }

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            String sql = "UPDATE genre SET name=?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, userInput);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Genre updated successfully");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
       return listGenres = getListOfGenres();
    }


    public boolean continueMethod(String str) {
        boolean continueLoop = true;
        String userinput = "";
        System.out.println(str + " y/n");

        boolean choiceIsOK = false;
        do{
            if (in.hasNextLine()) {
                userinput = in.nextLine();
            }

            char choice = userinput.toLowerCase().charAt(0);
            switch(choice){
                case 'y':
                    continueLoop = true;
                    choiceIsOK = true;

                    break;
                case 'n':
                    continueLoop = false;
                    choiceIsOK = true;
                    break;
                default:
                    System.out.println("Type Y or N to respectively continue or quit");
                    break;
            }
        }while(!choiceIsOK);
        return continueLoop;
    }

    private int validateInt() {
        int number;
        String mas = "";
        do {
            System.out.print(mas);
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" - isn`t number, try again: ", input);
            }
            number = in.nextInt();
            mas = "negative number, try again: ";
            in.nextLine();
        } while (number < 0);
        return number;
    }

    private String inputMore() {
        String name = "";
        do {
            if (in.hasNextLine()) {
                name = in.nextLine();
            }
            if (name.length() < 3) {
                System.out.println("Input more than 3 symbols");
            }
        } while (name.length() < 3);
        return name;
    }
}