package com.company;

import java.io.Serializable;

public class Book implements Serializable {
    private  int id;
    private String bookName;
    private int bookPages;
    private String bookAuthor;
    private int bookGenre;
    private String bookWrapper;
    private double bookPrice;
    private String userAdded;
    private String userEdited;

    //private static final long serialVersionUID = 1L;

    public Book (int id, String name, String author, int pages,
                 String wrapper, double price, String userAdded, String userEdited, int bookGenre){
        this.id = id;
        this.bookName = name;
        this.bookAuthor = author;
        this.bookWrapper = wrapper;
        this.bookPrice = price;
        this.bookPages = pages;
        this.userAdded = userAdded;
        this.userEdited = userEdited;
        this.bookGenre = bookGenre;
    }

    public Book(){}

    public void displayBookInfo(){
        System.out.printf("Book name: %s Book author: %s Book pages: %d Book genre: %s Book wrapper: %s Book price: %.2f Added: %s Last edited: %s\n",
                bookName, bookAuthor, bookPages, displayGenre(), bookWrapper, bookPrice, displayWhoAdded(), displayWhoEdited());
    }

    private String displayWhoAdded(){
        if (userAdded == null) {
            return "-";
        }
        return userAdded.getUsername();
    }

    private String displayWhoEdited(){
        if (userEdited == null) {
            return "-";
        }
        return userEdited.getUsername();
    }

    private String displayGenre(){
        if (bookGenre == 0) {
            return "-";
        }
        return GenreDatabaseUtility.listGenres.get(bookGenre);
    }


    public String getUserAdded() {
        return userAdded;
    }

    public void setUserAdded(String userAdded) {
        this.userAdded = userAdded;
    }

    public String getUserEdited() {
        return userEdited;
    }

    public void setUserEdited(String userEdited) {
        this.userEdited = userEdited;
    }

    public int getBookGenre() {
        return bookGenre;
    }

    public void setBookGenre(int bookGenre) {
        this.bookGenre = bookGenre;
    }

    public String getBookWrapper() {
        return bookWrapper;
    }

    public void setBookWrapper(String bookWrapper) {
        this.bookWrapper = bookWrapper;
    }

    public double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(double bookPrice) {
        this.bookPrice = bookPrice;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setBookPages(int bookPages) {
        this.bookPages = bookPages;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookName() {
        return bookName;
    }

    public int getBookPages() {
        return bookPages;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    private void log(String string) {
        System.out.println(string);
    }
}