package com.company;

public class User {
    private String name;
    private String surname;
    private String email;
    private String username;
    private int age;

    public User(String name, String surname, String email, String username, int age) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.username = username;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
