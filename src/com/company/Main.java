package com.company;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Main {

    static Scanner in = new Scanner(System.in);
    static ArrayList<Book> books = new ArrayList<>();
    static List<User> userList = new ArrayList<>();
    static List<Genre> genreList;
    static Gson gson = new Gson();
    private static String crunchify_file_location = "D:/java/book-storeclone/crunchify.txt";
    private static GenreDatabaseUtility gu = new GenreDatabaseUtility();
    //private static List<String> valuesFromGenreList;
    private static UserDatabaseUtility db = new UserDatabaseUtility();
    private static String loggedUser = "";
    private static String jdbcUrl = "jdbc:mysql://localhost:3306/bookstore?" + "autoReconnect=true&useSSL=false";
    private static String dbusername = "root";
    private static String pass = "pass";
    private static Connection connection;

    public static void main(String[] args) throws IOException {
        loggedUser = doLoginIntoMenu();
        if (loggedUser == null){
            System.exit(0);
        }

        userList = db.loadUsersFromDB();
        genreList = gu.getListOfGenres();
        //valuesFromGenreList = new ArrayList(genreList.values());
        if (genreList == null) {
            System.out.println(" NULL ");
            genreList = new ArrayList<>();
        } else {
            log(genreList.size() + " SIZE ");
        }
        crunchifyReadFromFile();
        listBook();

        String command = "";
        boolean otpusti = true;

        while (otpusti) {
            System.out.println("***** Add list search get edit del genres exit *****");

            if (in.hasNextLine()) {
                command = in.nextLine();
            }

            switch (command) {
                case "add":
                    addBook();
                    break;

                case "list":
                    listBook();
                    break;

                case "search":
                    searchBook();
                    break;

                case "get":
                    getBook();
                    break;

                case "edit":
                    editBook();
                    break;

                case "del":
                    deleteBook();
                    break;

                case "genres":
                    genresMenu();
                    break;

                case "exit":
                    otpusti = false;
                    break;

                default:
                    System.out.println("Hi bruh");
            }
        }
    }

    //---------------------------------- DB

    private static String getUserWhoAddedOrEditBook(){
        for (User u : userList) {
            if (u.getUsername().equals(loggedUser)) {
                return u.getName();
            }
        }
        return null;
    }

    private static String doLoginIntoMenu(){
        String loggedUser = null;
        UserDatabaseUtility du = new UserDatabaseUtility();

        String command = "";
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("***** Register login exit *****");

            if (in.hasNextLine()) {
                command = in.nextLine();
            }

            switch (command) {
                case "register":
                    du.doRegister();
                    break;
                case "login":
                    loggedUser = du.doLogin();
                    return loggedUser;
                case "exit":
                    return loggedUser;
                default:
                    System.out.println("Choose the right choice");
            }
            continueLoop = continueMethod("Continue?");
        }
        return loggedUser;
    }

    public  ArrayList<Book> getBooksFromDB(){
        ArrayList<Book> booksFromDb = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            String sql = "SELECT * FROM book";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            while (result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");;
                int pages = result.getInt("npages");
                String author = result.getString("author");
                String wrapper = result.getString("wrapper");
                double price = result.getDouble("price");
                String useradded = result.getString("useradded");
                String useredited = result.getString("useredited");
                int genre = result.getInt("genre");

                Book book = new Book(id, name, author, pages, wrapper, price, useradded, useredited, genre);
                booksFromDb.add(book);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return booksFromDb;
    }

    public ArrayList<Book> getListOfBooks(){
        books =  getBooksFromDB();
        return books;
    }

    //---------------------------------------- GENRES

    private static void genresMenu() {
        String command = "";
        boolean otpusti = true;

        while (otpusti) {
            System.out.println("**GENRES MENU: Add del list edit exit *****");

            if (in.hasNextLine()) {
                command = in.nextLine().toLowerCase();
            }

            switch (command) {
                case "add":
                    genreList = gu.addGenre();
                    break;

                case "del":
                    int choice = showBooksIfDeleteGenre();
                    genreList = gu.deleteGenre(choice);
                    eraseGenre(choice);
                    break;

                case "list":
                    listGenres();
                    break;

                case "edit":
                    genreList = gu.editGenre();
                    //valuesFromGenreList = new ArrayList(genreList.values());
                    break;

                case "exit":
                    otpusti = false;
                    break;

                default:
                    System.out.println("Choose the right choice");
            }
        }
    }

    public static  void listGenres(){
        int i = 1;
        if (!genreList.isEmpty()) {
            for (Genre g:genreList) {
                System.out.println(i + ". " + g.getName());
                i++;
            }
        } else {
            System.out.println("No genres");
        }
    }

    private static ArrayList<String> getGenresToList(){
        ArrayList<String> genres=  new ArrayList<>();
        for (Genre g:genreList) {
            genres.add(g.getName());
        }
        return genres;
    }

    private static int chooseNewGenre(Book book) {
        boolean passed;
        do {
            System.out.println("-- Choose new genre:");
            listGenres();
            int userChoose = validateInt() - 1;

            try {
                String s = genreList.get(userChoose);
                for (Map.Entry entry : genreList.entrySet()) {
                    if (entry.getValue().equals(s)) {
                        book.setBookGenre((int) entry.getKey());
                    }
                }
                passed = true;
            } catch (IndexOutOfBoundsException e) {
                System.out.println("No genre with such id");
                passed = false;
            }
        }while (!passed);
        return
    }

    private static int showBooksIfDeleteGenre(){
        int bookGenre = 0;
        String s = "";
        String s1 = "";
        System.out.println("Choose genre id");
        listGenres();
        int userChoice = validateInt()-1;
        try {
            s = valuesFromGenreList.get(userChoice);
            for (Map.Entry entry : genreList.entrySet()) {
                if (entry.getValue().equals(s)) {
                    bookGenre = (int) entry.getKey();
                }
            }

            System.out.println("Warning, after deleting " + s + " this genre will be erased in those books, input name of the genre to  delete or 'not' to cencel: ");
            for (Book book : books){
                if (book.getBookGenre()==bookGenre) {
                    book.displayBookInfo();
                }
            }
            System.out.print("--> " );
            if (in.hasNextLine()) {
                s1 = in.nextLine();
            }

            if (s1.toLowerCase().equals(s)) {
                return bookGenre;
            } else if (s1.equals("not")) {
                System.out.println("Cencel");
            } else {
                System.out.println("Incorrect name");
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("No genre with such id");
        }
        return 0;
    }

    private static void eraseGenre(int bookGenre){
        if (bookGenre != 0) {
            for (Book b : books) {
                if (b.getBookGenre() == bookGenre) {
                    b.setBookGenre(0);
                }
            }
            saveBook1(books);
        }
    }

    private static Book bookEdit(Book book) {
        boolean continueLoop = true;
        while (continueLoop) {

            System.out.println("What to edit");
            System.out.println("1\t Name");
            System.out.println("2\t Author");
            System.out.println("3\t Number of pages");
            System.out.println("4\t Genre");
            System.out.println("5\t Cover");
            System.out.println("6\t Price");
            System.out.println("7\t Nothing");

            int menuItem;

            menuItem = in.nextInt();
            in.nextLine();

            switch (menuItem) {
                case 1:
                    System.out.println("New name: ");
                    if (in.hasNextLine()) {
                        book.setBookName(in.nextLine());
                    }
                    break;
                case 2:
                    System.out.println("New author: ");
                    if (in.hasNextLine()) {
                        book.setBookAuthor(in.nextLine());
                    }
                    break;
                case 3:
                    System.out.println("New n of pages: ");
                    book.setBookPages(validateInt());
                    break;
                case 4:
                    chooseNewGenre(book);
                    break;
                case 5:
                    System.out.println("New cover: ");
                    if (in.hasNextLine()) {
                        book.setBookWrapper(in.nextLine());
                    }
                    break;
                case 6:
                    System.out.println("New price: ");
                    book.setBookPrice(validateDouble());
                    break;
                case 7:
                    return book;

                default:
                    System.out.println("Choose the right choice");
            }
            continueLoop = continueMethod("Edit another parameter? ");
        }
        book.setUserEdited(getUserWhoAddedOrEditBook());
        return book;
    }

    private static void editBook() {
        boolean continueLoop = true;
        while (continueLoop) {
            int index;
            Book book = new Book();
            System.out.println("Inpu N of the book:");
            index = validateInt() - 1;
            if (index < books.size() && index >= 0) {
                book = books.get(index);
                System.out.print("Current book: ");
                book.displayBookInfo();
                bookEdit(book);
            } else {
                System.out.println("No such books");
            }

            saveBook1(books);
            continueLoop = continueMethod("Edit more books? ");
        }
    }


    private static void getBook() {
        boolean continueLoop = true;
        while (continueLoop) {
            int index;
            Book book = new Book();
            System.out.println("Введите номер книги: ");

            index = validateInt() - 1;
            if (index < books.size() && index >= 0) {
                book = books.get(index);
                book.displayBookInfo();
            } else {
                System.out.println("No such books");
            }
        }
        continueLoop = continueMethod("Find more books? ");
    }


    public static void listBook() {
        if (!books.isEmpty()) {
            for (Book b : books) {
                b.displayBookInfo();
            }
        } else {
            System.out.println("No books");
        }
    }

    public static void addBook() {
        boolean continueLoop = true;
        while (continueLoop) {
            Book book = new Book();
            System.out.println("Input name: ");
            if (in.hasNextLine()) {
                book.setBookName(in.nextLine());
            }

            System.out.println("Input author: ");
            if (in.hasNextLine()) {
                book.setBookAuthor(in.nextLine());
            }

            System.out.println("Input n of pages: ");
            book.setBookPages(validateInt());

            chooseNewGenre(book);

            System.out.println("Input cover of the book: ");
            if (in.hasNextLine()) {
                book.setBookWrapper(in.nextLine());
            }

            System.out.println("Input price: ");
            book.setBookPrice(validateDouble());

            book.setUserAdded(getUserWhoAddedOrEditBook());

            try {
                connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
                String sql = "INSERT INTO users (name, surname, email, age, username, password) " +
                        "VALUES ('" + name + "', '" + surname + "', '" + email + "', '" + age + "', '" + username + "', MD5('" + passw + "'))";
                Statement statement = connection.createStatement();
                int rows = statement.executeUpdate(sql);
                if (rows > 0) {
                    System.out.println("Registration complete");
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            continueLoop = continueMethod("Add more mbooks? ");
        }
    }

    public static void saveBook(List<Book> book) {
        try {
            FileOutputStream fos = new FileOutputStream("bookss1");
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(book);
            os.close();
            fos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void saveBook1(List<Book> book) {
        crunchifyWriteToFile(gson.toJson(book));
    }

    public static void crunchifyReadFromFile() {
        File crunchifyFile = new File(crunchify_file_location);
        if (!crunchifyFile.exists())
            log("File doesn't exist");

        InputStreamReader isReader;
        try {
            isReader = new InputStreamReader(new FileInputStream(crunchifyFile), "UTF-8");

            JsonReader myReader = new JsonReader(isReader);
            books = gson.fromJson(myReader, new TypeToken<List<Book>>() {
            }.getType());
            if (books == null){
                books = new ArrayList<>();
            }

        } catch (Exception e) {
            log("error load cache from file " + e.toString());
        }

        log("\nBooks loaded successfully from file " + crunchify_file_location);

    }

    public static void crunchifyWriteToFile(String myData) {
        File crunchifyFile = new File(crunchify_file_location);
        if (!crunchifyFile.exists()) {
            try {
                File directory = new File(crunchifyFile.getParent());
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                crunchifyFile.createNewFile();
            } catch (IOException e) {
                log("Excepton Occured: " + e.toString());
            }
        }

        try {
            // Convenience class for writing character files
            FileWriter crunchifyWriter;
            crunchifyWriter = new FileWriter(crunchifyFile.getAbsoluteFile());

            // Writes text to a character-output stream
            BufferedWriter bufferWriter = new BufferedWriter(crunchifyWriter);
            bufferWriter.write(myData.toString());
            bufferWriter.close();

            log("Book saved at file location: " + crunchify_file_location + "\n");
        } catch (IOException e) {
            log("Hmm.. Got an error while saving book to file " + e.toString());
        }
    }

    public static void getBooks() {
        try (FileInputStream fin = new FileInputStream("bookss1")) {
            ObjectInputStream oi = new ObjectInputStream(fin);
            books = (ArrayList) oi.readObject();
            oi.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static boolean continueMethod(String str) {
        boolean continueLoop = true;
        String userinput = "";
        System.out.println(str + " y/n");

//        String a = "";
//
//        if (in.hasNextLine()) {
//            a = in.nextLine();
//            if (a.equals("y")) {
//                continueLoop = true;
//            } else if (a.equals("n")) {
//                continueLoop = false;
//            } else {
//                System.out.println("FATAL ERROR");
//                continueLoop = false;
//            }
//        }
        boolean choiceIsOK = false;
        do{
            if (in.hasNextLine()) {
                userinput = in.nextLine();
            }

            char choice = userinput.toLowerCase().charAt(0);
            switch(choice){
                case 'y':
                    continueLoop = true;
                    choiceIsOK = true;

                    break;
                case 'n':
                    continueLoop = false;
                    choiceIsOK = true;
                    break;
                default:
                    // error or warning
                    System.out.println("Type Y or N to respectively continue or quit");
                    break;
            }
        }while(!choiceIsOK);

        return continueLoop;
    }
    // -----------------------------------------------  ПОИСК

    public static void searchBook() {
        boolean continueLoop = true;
        while (continueLoop) {

            System.out.println("** How to search books? ");
            System.out.println("1\t By name");
            System.out.println("2\t By author");
            System.out.println("3\t By n of pages");
            System.out.println("4\t By genre");
            System.out.println("5\t By cover");
            System.out.println("6\t By price");
            System.out.println("7\t Dont search");

            int menuItem;

            menuItem = in.nextInt();
            in.nextLine();

            switch (menuItem) {
                case 1:
                    searchBooksByName();
                    break;
                case 2:
                    searchBooksByAuthor();
                    break;
                case 3:
                    searchBooksByPages();
                    break;
                case 4:
                    searchBooksByGenre();
                    break;
                case 5:
                    searchBooksByWrapper();
                    break;
                case 6:
                    searchBooksByPrice();
                    break;
                case 7:
                    return;

                default:
                    System.out.println("Choose the right choice");
            }
            continueLoop = continueMethod("** Find more books? ");
        }
    }

    public static void searchBooksByName() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input name of the book");
            String name = inputMore();

            for (Book b : books) {
                if (b.getBookName() != null && b.getBookName().toLowerCase().contains(name.toLowerCase())) {
                    b.displayBookInfo();
                }
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    public static void searchBooksByPages() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input min number of pages");
            int minP =  validateInt();

            System.out.println("Input max number of pages");
            int maxP =  validateInt();

            for (Book b : books) {
                if (b.getBookPages() != 0 && b.getBookPages() >= minP && b.getBookPages() <= maxP) {
                    b.displayBookInfo();
                }
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    public static void searchBooksByAuthor() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input author");
            String author = inputMore();

            for (Book b : books) {
                if (b.getBookAuthor() != null && b.getBookAuthor().contains(author)) {
                    b.displayBookInfo();
                }
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    public static void searchBooksByGenre() {
        int bookGenre = 0;
        boolean isFound = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("** Choose genre");
            listGenres();
            int userChoose = validateInt()-1;
            String s = valuesFromGenreList.get(userChoose);

            for (Map.Entry entry : genreList.entrySet()) {
                if (entry.getValue().equals(s)) {
                    bookGenre = (int) entry.getKey();
                }
            }
            for (Book b : books) {
                if (b.getBookGenre() != 0 && b.getBookGenre() == bookGenre) {
                    b.displayBookInfo();
                    isFound = true;
                }
            }
            if (!isFound) {
                System.out.println("Books not found");
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    public static void searchBooksByWrapper() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input book cover");
            String wrapper = "";

            if (in.hasNextLine()) {
                wrapper = in.nextLine();
            }

            for (Book b : books) {
                if (b.getBookWrapper() != null && b.getBookWrapper().contains(wrapper)) {
                    b.displayBookInfo();
                }
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    public static void searchBooksByPrice() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input min price");
            double minP = validateDouble();

            System.out.println("Input max price");
            double maxP = validateDouble();

            for (Book b : books) {
                if (b.getBookPrice() != 0 && b.getBookPrice() >= minP && b.getBookPrice() <= maxP) {
                    b.displayBookInfo();
                }
            }
            continueLoop = continueMethod("Find more books? ");
        }
    }

    // -----------------------------------------------  УДАЛЕНИЕ

    public static void deleteBook() {
        boolean continueLoop = true;
        while (continueLoop) {

            System.out.println("** How to delete? ");
            System.out.println("1\t By name");
            System.out.println("2\t Be author");
            System.out.println("3\t By n of pages");
            System.out.println("4\t By genre");
            System.out.println("5\t By book cover");
            System.out.println("6\t By price");
            System.out.println("7\t Dont delete");

            int menuItem;

            menuItem = in.nextInt();
            in.nextLine();

            switch (menuItem) {
                case 1:
                    deleteBooksByName();
                    break;
                case 2:
                    deleteBooksByAuthor();
                    break;
                case 3:
                    deleteBooksByPages();
                    break;
                case 4:
                    deleteBooksByGenre();
                    break;
                case 5:
                    deleteBooksByWrapper();
                    break;
                case 6:
                    deleteBooksByPrice();
                    break;
                case 7:
                    return;

                default:
                    System.out.println("Input the right choice");
            }
            continueLoop = continueMethod("** Delete more books? ");
        }
    }

    public static void deleteBooksByName() {
        List<Book> booksToDelete = new ArrayList<>();
        boolean isDeleted = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input name of the book");
            String name = inputMore();

            for (Book b : books) {
                if (b.getBookName() != null && b.getBookName().toLowerCase().contains(name.toLowerCase())) {
                    isDeleted = true;
                    booksToDelete.add(b);
                }
            }

            if (!isDeleted) {
                System.out.println("Books not found");
            } else {
                deleteBooks(booksToDelete);
            }

            saveBook1(books);
            continueLoop = continueMethod("Delete more books? ");
        }
    }

    public static void deleteBooksByAuthor() {
        List<Book> booksToDelete = new ArrayList<>();
        boolean isDeleted = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input author");
            String author = inputMore();

            for (Book b : books) {
                if (b.getBookAuthor() != null && b.getBookAuthor().toLowerCase().contains(author.toLowerCase())) {
                    isDeleted = true;
                    booksToDelete.add(b);
                }
            }

            if (!isDeleted) {
                System.out.println("Books not found");
            } else {
                deleteBooks(booksToDelete);
            }
            saveBook1(books);
            continueLoop = continueMethod("Delete more books? ");
        }
    }

    public static void deleteBooksByPages() {
        List<Book> booksToDelete = new ArrayList<>();
        boolean isDeleted = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input min number of pages");
            int minP = validateInt();

            System.out.println("Input max number of pages");
            int maxP = validateInt();

            for (Book b : books) {
                if (b.getBookPages() != 0 && b.getBookPages()>= minP && b.getBookPages() <= maxP) {
                    isDeleted = true;
                    booksToDelete.add(b);
                }
            }
            if (!isDeleted) {
                System.out.println("Books not found");
            } else {
                deleteBooks(booksToDelete);
            }

            saveBook1(books);
            continueLoop = continueMethod("Delete more books? ");
        }
    }

    public static void deleteBooksByGenre() {
        List<Book> booksToDelete = new ArrayList<>();
        int bookGenre = 0;
        boolean isDeleted = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("** Choose genre");
            listGenres();
            int userChoose = validateInt()-1;
            String s = valuesFromGenreList.get(userChoose);

            for (Map.Entry entry : genreList.entrySet()) {
                if (entry.getValue().equals(s)) {
                    bookGenre = (int) entry.getKey();
                    System.out.println(bookGenre + "/....");
                }
            }
                for (Book b : books) {
                    if (b.getBookGenre() != 0 && b.getBookGenre() == bookGenre) {
                        System.out.println(b.getBookGenre() +  " .. " + bookGenre);
                        isDeleted = true;
                        booksToDelete.add(b);
                    }
                }
                if (!isDeleted) {
                    System.out.println("Books not found");
                } else {
                    deleteBooks(booksToDelete);
                }
            continueLoop = continueMethod("Delete more books? ");
            }
            saveBook1(books);
    }

    public static void deleteBooksByWrapper() {
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input book cover");
            String wrapper = "";

            if (in.hasNextLine()) {
                wrapper = in.nextLine();
            }

//            for (int i = 0; i < books.size(); i++) {
//                if (books.get(i).getBookWrapper().contains(wrapper)) {
//                    books.remove(i);
//                    System.out.println("removed");
//                }
//            }
            saveBook1(books);
            continueLoop = continueMethod("Delete more books? ");
        }
    }

    public static void deleteBooksByPrice() {
        List<Book> booksToDelete = new ArrayList<>();
        boolean isDeleted = false;
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input min price");
            double minP = validateDouble();

            System.out.println("Input max price");
            double maxP = validateDouble();

            for (Book b : books) {
                if (b.getBookPages() != 0 && b.getBookPrice()>= minP && b.getBookPrice() <= maxP) {
                    isDeleted = true;
                    booksToDelete.add(b);
                }
            }
            if (!isDeleted) {
                System.out.println("Books not found");
            } else {
                deleteBooks(booksToDelete);
            }

            saveBook1(books);
            continueLoop = continueMethod("Delete more books? ");
        }
    }

    private static void log(String string) {
        System.out.println(string);
    }

//    private static void patternDec(){
//        Pattern pattern = new Pattern("[A-Za-z]{1,20}");
//        if ((pattern.matcher(s).matches()))
//        { System.out.println("111");};
//    }

    private static int validateInt() {
        int number;
        String mas = "";
        do {
            System.out.print(mas);
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" - isn`t number, try again: ", input);
            }
            number = in.nextInt();
            mas = "negative number, try again: ";
            in.nextLine();
        } while (number < 0);

        return number;
    }

    private static double validateDouble() {
        double number;
        String mas = "";
        do {
            System.out.print(mas);
            while (!in.hasNextDouble()) {
                String input = in.next();
                System.out.printf("\"%s\" - isn`t number, try again: ", input);
            }
            number = in.nextDouble();
            mas = "negative number, try again: ";
            in.nextLine();
        } while (number < 0);

        return number;
    }

    private static String inputMore(){
        String name = "";
        do {
            if (in.hasNextLine()) {
                name = in.nextLine();
            }
            if (name.length()<3){
                System.out.println("Введите больше 3-х символов");
            }
        } while(name.length()<3);

        return name;
    }

    private static void deleteBooks(List<Book> booksToDelete){
        System.out.println("------ Want to delete? y/n-----");
        for (Book b : booksToDelete){
            b.displayBookInfo();
        }
        boolean choiceIsOK = false;
        String userinput = "";
        do {
            if (in.hasNextLine()) {
                userinput = in.nextLine();
            }

            char choice = userinput.toLowerCase().charAt(0);
            switch (choice) {
                case 'y':
                    books.removeAll(booksToDelete);
                    System.out.println("Books deleted");
                    choiceIsOK = true;
                    booksToDelete.clear();
                    break;
                case 'n':
                    choiceIsOK = true;
                    booksToDelete.clear();
                    break;
                default:
                    // error or warning
                    System.out.println("Type Y or N to respectively continue or quit");
                    break;
            }
        } while (!choiceIsOK);
    }
}