package com.company;

import java.security.MessageDigest;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class UserDatabaseUtility {
    Scanner in = new Scanner(System.in);
    String jdbcUrl = "jdbc:mysql://localhost:3306/bookstore?" + "autoReconnect=true&useSSL=false";
    String dbusername = "root";
    String pass = "pass";
    Connection connection;

    public void doConnect() {
        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            if (connection != null) {
                System.out.println("Connected to db");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void doRegister() {
        boolean passed;
        String username = "";

            System.out.println("Input Name");
            String name = inputMore();

            System.out.println("Input Surname");
            String surname = inputMore();

            System.out.println("Input Email");
            String email = inputMore();

            System.out.println("Input Age");
            int age = validateInt();

            do {
                System.out.println("Input Unique Username");
                username = inputMore();
                if (checkSameUsername(username)) {
                    passed = true;
                    System.out.println("Username is already exist");
                } else {
                    break;
                }
            } while (passed);

                System.out.println("Input password");
            String passw = inputMore();

            try {
                connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
                String sql = "INSERT INTO users (name, surname, email, age, username, password) " +
                        "VALUES ('" + name + "', '" + surname + "', '" + email + "', '" + age + "', '" + username + "', MD5('" + passw + "'))";
                Statement statement = connection.createStatement();
                int rows = statement.executeUpdate(sql);
                if (rows > 0) {
                    System.out.println("Registration complete");
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public String doLogin() {
        String loggedUser = null;
        String sql = "";
        String usernameInput = "", passInput = "";

        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("Input Username");
            usernameInput = inputMore();

            System.out.println("Input Password");
            passInput = inputMore();
            passInput = doDecrypt(passInput);

            try {
                connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
                sql = "SELECT * FROM users WHERE username = '" + usernameInput + "' AND password = '" + passInput + "'";
                Statement statement = connection.createStatement();

                ResultSet result = statement.executeQuery(sql);
                if (result.next()) {
                    loggedUser = usernameInput;
                    break;
                }

                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (!checkCorrectPassword(passInput) && !checkCorrectUsername(usernameInput)) {
                System.out.println("Incorrect username and pass");
            } else if (!checkCorrectUsername(usernameInput)) {
                System.out.println("Incorrect username");
            } else if (!checkCorrectPassword(passInput)) {
                System.out.println("Incorrect pass");
            }

            continueLoop = continueMethod("Try again?");
        }
        return loggedUser;
    }

    //check correct username
    private boolean checkCorrectUsername(String username){
        String sql = "";
        boolean isExist = false;

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            sql = "SELECT * FROM users WHERE username = '" + username + "'";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                isExist = true;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    //check correct pass
    private boolean checkCorrectPassword(String passw){
        String sql = "";
        boolean isExist = false;

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            sql = "SELECT * FROM users WHERE password = '" + passw + "'";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                isExist = true;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    // check login for registration
    private boolean checkSameUsername(String username){
        String sql = "";
        boolean isExist = false;

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            sql = "SELECT * FROM users WHERE username = '" + username + "'";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                isExist = true;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    public ArrayList<User> loadUsersFromDB(){
        ArrayList<User> users = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(jdbcUrl, dbusername, pass);
            String sql = "SELECT * FROM users";
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);
            while (result.next()){
                String name = result.getString("name");
                String surname = result.getString("surname");
                String email = result.getString("email");
                int age = result.getInt("age");
                String username = result.getString("username");
                User user = new User(name, surname, email, username, age);
                users.add(user);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    private String inputMore() {
        String name = "";
        do {
            if (in.hasNextLine()) {
                name = in.nextLine();
            }
            if (name.length() < 3) {
                System.out.println("Input more than 3 symbols");
            }
        } while (name.length() < 3);
        return name;
    }

    public boolean continueMethod(String str) {
        boolean continueLoop = true;
        String userinput = "";
        System.out.println(str + " y/n");

        boolean choiceIsOK = false;
        do {
            if (in.hasNextLine()) {
                userinput = in.nextLine();
            }

            char choice = userinput.toLowerCase().charAt(0);
            switch (choice) {
                case 'y':
                    continueLoop = true;
                    choiceIsOK = true;

                    break;
                case 'n':
                    continueLoop = false;
                    choiceIsOK = true;
                    break;
                default:
                    // error or warning
                    System.out.println("Type Y or N to respectively continue or quit");
                    break;
            }
        } while (!choiceIsOK);

        return continueLoop;
    }

//    public  String doEncrypt(String password){
//        StringBuilder builder = null;
//        try {
//            MessageDigest md5 = MessageDigest.getInstance("MD5");
//            byte[] bytes = md5.digest(password.getBytes());
//            builder = new StringBuilder();
//            for (byte b: bytes){
//                builder.append(String.format("%02x", b));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return builder.toString();
//    }

    public String doDecrypt(String password) {
        StringBuilder builder = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(password.getBytes());
            builder = new StringBuilder();
            for (byte b : bytes) {
                builder.append(String.format("%02x", b));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private int validateInt() {
        int number;
        String mas = "";
        do {
            System.out.print(mas);
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" - isn`t number, try again: ", input);
            }
            number = in.nextInt();
            mas = "negative number, try again: ";
            in.nextLine();
        } while (number < 0);

        return number;
    }
}